Source: loki-ecmwf
Section: python
Priority: optional
Maintainer: Debian Science Maintainers <debian-science-maintainers@lists.alioth.debian.org>
Uploaders: Alastair McKinstry <mckinstry@debian.org>
Build-Depends: debhelper-compat (= 13),
 dh-sequence-python3,
 dh-sequence-sphinxdoc <!nodoc>,
 python3-setuptools,
 python3-setuptools-scm,
 python3-setuptools-whl,
 pybuild-plugin-pyproject,
 python3-all, 
 python3-numpy,
 python3-codetiming,
 python3-pcpp,
 python3-pydantic,
 python3-pymbolic,
 python3-fparser,
 python3-nbconvert <!nocheck>,
 python3-pandas <!nocheck>,
 python3-pytest <!nocheck>,
 python3-pytest-cov <!nocheck>,
 pylint <!nocheck>,
 pandoc <!nodoc>,
 python3-sphinx <!nodoc>,
 python3-nbsphinx <!nodoc>,
 python3-sphinx-rtd-theme <!nodoc>,
 python3-recommonmark <!nodoc>,
 python3-myst-parser <!nodoc>,
 python3-sphinx-design <!nodoc>
X-Python3-Version: >= 3.10
Standards-Version: 4.7.0
Homepage: https://github.com/ecmwf-ifs/loki
Vcs-Browser: https://salsa.debian.org/science-team/loki-ecmwf.git
Vcs-Git: https://salsa.debian.org/science-team/loki-ecmwf.git

Package: python3-loki-ecmwf
Architecture: all
Depends: ${python3:Depends}, ${misc:Depends}
Recommends: python3-loki-ecmwf-doc,
 python3-loki-ecmwf-transformations,
 python3-loki-ecmwf-lint-rules
Description: Source-to-source translation for Fortran
 Loki is an experimental tool to explore the possible use of source-to-source
 translation for ECMWF's Integrated Forecasting System (IFS) and associated
 Fortran software packages.
 .
 Loki is based on compiler technology (visitor patterns and ASTs) and aims
 to provide an abstract, language-agnostic representation of a kernel,
 as well as a programmable (pythonic) interface that allows developers
 to experiment with different kernel implementations and optimizations.
 The aim is to allow changes to programming models and coding styles
 to be encoded and automated instead of hand-applying them, enabling
 advanced experimentation with large kernels as well as bulk processing
 of large numbers of source files to evaluate different kernel implementations
 and programming models.

Package: python3-loki-ecmwf-doc
Architecture: all
Depends:  ${sphinxdoc:Depends},
	 ${misc:Depends}
Built-Using: ${sphinxdoc:Built-Using}
Description: Source-to-source translation for Fortran - documentation
 Loki is an experimental tool to explore the possible use of source-to-source
 translation for ECMWF's Integrated Forecasting System (IFS) and associated
 Fortran software packages.
 .
 This package contains the HTML documentation for Loki.

Package: python3-loki-ecmwf-transformations
Architecture: all
Depends: ${misc:Depends}
Description: Source-to-source translation for Fortran - IFS transformations
 Loki is an experimental tool to explore the possible use of source-to-source
 translation for ECMWF's Integrated Forecasting System (IFS) and associated
 Fortran software packages.
 .
 This package contains the IFS transformation implementations for Loki.

Package: python3-loki-ecmwf-lint-rules
Architecture: all
Depends:  ${misc:Depends}
Description: Source-to-source translation for Fortran - Lint rules
 Loki is an experimental tool to explore the possible use of source-to-source
 translation for ECMWF's Integrated Forecasting System (IFS) and associated
 Fortran software packages.
 .
 This package contains the Lint rules for the loki-lint tool.
