#!/usr/bin/make -f

# Uncomment this to turn on verbose mode.
export DH_VERBOSE=1

export PYBUILD_SYSTEM=pyproject
export SETUPTOOLS_SCM_PRETEND_VERSION=$(DEB_VERSION_UPSTREAM)

export DEB_BUILD_MAINT_OPTIONS = hardening=+all
DPKG_EXPORT_BUILDFLAGS = 1
include /usr/share/dpkg/buildflags.mk
include /usr/share/dpkg/pkg-info.mk

DESTDIR:=$(CURDIR)/debian/python3-loki-ecmwf

PYTHONPATH:=$(CURDIR)
export PYTHONPATH

# magic debhelper rule
%:
	dh $@ --buildsystem=pybuild  --with python3,sphinxdoc 

override_dh_auto_configure:
	dh_auto_configure
	# 'scripts' is too generic
	mv scripts loki

override_dh_auto_build:
	PYBUILD_NAME=loki-ecmwf dh_auto_build -O--buildsystem=pybuild
	# transformations no longer a project
	# PYBUILD_NAME=loki-ecmwf-transformations	dh_auto_build -O--buildsystem=pybuild --sourcedirectory=loki/transformations
	PYBUILD_NAME=loki-ecmwf-lint-rules dh_auto_build -O--buildsystem=pybuild --sourcedirectory=lint_rules
	PYTHONPATH=$(CURDIR) $(MAKE) -C docs html

override_dh_auto_install:
	PYBUILD_NAME=loki-ecmwf	dh_auto_install -O--buildsystem=pybuild
	PYBUILD_NAME=loki-ecmwf-transformations	dh_auto_install -O--buildsystem=pybuild --sourcedirectory=loki/transformations
	PYBUILD_NAME=loki-ecmwf-lint-rules	dh_auto_install -O--buildsystem=pybuild --sourcedirectory=lint_rules
	mv $(DESTDIR)/usr/bin/loki-lint.py  $(DESTDIR)/usr/bin/loki-lint
	mv $(DESTDIR)/usr/bin/loki-transform.py $(DESTDIR)/usr/bin/loki-transform

ifeq (,$(filter nodoc,$(DEB_BUILD_OPTIONS)))
override_dh_sphinxdoc:
	dh_sphinxdoc --exclude=MathJax.js
	find debian/python3-loki-ecmwf-doc -name '*.html' \
	 -exec grep require.js {} /dev/null \; | cut -f1 -d: | while read r; do \
	sed -e 's%https://cdnjs.cloudflare.com/ajax/libs/require.js/2.3.4% file:/usr/share/javascript/requirejs%' \
	< $$r > debian/tmp/x ; \
	mv debian/tmp/x $$r ; \
	done
endif

override_dh_auto_test:
	( dh_auto_test || echo "Don't fail on tests; just capture errors" )
